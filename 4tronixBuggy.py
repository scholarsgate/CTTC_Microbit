
from microbit import *

leftA = pin0
leftB = pin8
rightA = pin1
rightB = pin12

def forward(speedpct):
    speed = speedpct * 10.23
    leftA.write_analog(speed)
    leftB.write_digital(0)
    rightA.write_analog(speed)
    rightB.write_digital(0)

def reverse(speedpct):
    speed = speedpct * 10.23
    leftA.write_analog(1023-speed)
    leftB.write_digital(1)
    rightA.write_analog(1023-speed)
    rightB.write_digital(1)

def spinRight(speedpct):
    speed = speedpct * 10.23
    leftA.write_analog(1023-speed)
    leftB.write_digital(1)
    rightA.write_analog(speed)
    rightB.write_digital(0)

def spinLeft(speedpct):
    speed = speedpct * 10.23
    leftA.write_analog(speed)
    leftB.write_digital(0)
    rightA.write_analog(1023-speed)
    rightB.write_digital(1)

def stop():
    leftA.write_analog(0)
    leftB.write_digital(0)
    rightA.write_analog(0)
    rightB.write_digital(0)

display.show("5")
sleep(1000)
display.show("4")
sleep(1000)
display.show("3")
sleep(1000)
display.show("2")
sleep(1000)
display.show("1")
sleep(1000)
display.show(Image.ARROW_S)
forward(40)
sleep(3000)
display.show(Image.ARROW_E)
spinRight(40)
sleep(3000)
display.show(Image.ARROW_N)
reverse(40)
sleep(3000)
display.show(Image.ARROW_W)
spinLeft(40)
sleep(3000)
display.show(Image.HAPPY)
stop()
